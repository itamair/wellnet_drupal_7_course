<?php

$view = new view();
$view->name = 'geocontents_map';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Geocontents Map (in code)';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Geocontents Map';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'leggi tutto';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Applica';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Ripristina';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordina per';
$handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Disc';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'ip_geoloc_map';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => '',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['ip_geoloc_views_plugin_latitude'] = 'field_geofield';
$handler->display->display_options['style_options']['ip_geoloc_views_plugin_longitude'] = 'field_geofield';
$handler->display->display_options['style_options']['differentiator']['differentiator_field'] = 'field_tipo_geocontent';
$handler->display->display_options['style_options']['center_option'] = '3';
$handler->display->display_options['style_options']['gps_roles'] = array(
  1 => 0,
  2 => 0,
  3 => 0,
);
$handler->display->display_options['style_options']['map_options'] = '{"zoom": 3, "minZoom": 3, "maxZoom": 16}';
$handler->display->display_options['style_options']['map_div_style'] = 'height:600px';
/* No results behavior: Globale: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = '<h3>Ancora nessun Contenuto inputato sulla mappa !</h3>';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Campo: Contenuto: Titolo */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_type'] = 'strong';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Campo: Contenuto: Tipo */
$handler->display->display_options['fields']['field_tipo_geocontent']['id'] = 'field_tipo_geocontent';
$handler->display->display_options['fields']['field_tipo_geocontent']['table'] = 'field_data_field_tipo_geocontent';
$handler->display->display_options['fields']['field_tipo_geocontent']['field'] = 'field_tipo_geocontent';
$handler->display->display_options['fields']['field_tipo_geocontent']['element_label_type'] = 'strong';
$handler->display->display_options['fields']['field_tipo_geocontent']['type'] = 'taxonomy_term_reference_plain';
/* Campo: Contenuto: Immagine */
$handler->display->display_options['fields']['field_immagine']['id'] = 'field_immagine';
$handler->display->display_options['fields']['field_immagine']['table'] = 'field_data_field_immagine';
$handler->display->display_options['fields']['field_immagine']['field'] = 'field_immagine';
$handler->display->display_options['fields']['field_immagine']['label'] = '';
$handler->display->display_options['fields']['field_immagine']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_immagine']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_immagine']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
$handler->display->display_options['fields']['field_immagine']['delta_limit'] = '1';
$handler->display->display_options['fields']['field_immagine']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_immagine']['separator'] = '';
/* Campo: Contenuto: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = 'Descrizione';
$handler->display->display_options['fields']['body']['alter']['max_length'] = '100';
$handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
$handler->display->display_options['fields']['body']['element_label_type'] = 'strong';
/* Campo: Contenuto: Geofield */
$handler->display->display_options['fields']['field_geofield']['id'] = 'field_geofield';
$handler->display->display_options['fields']['field_geofield']['table'] = 'field_data_field_geofield';
$handler->display->display_options['fields']['field_geofield']['field'] = 'field_geofield';
$handler->display->display_options['fields']['field_geofield']['label'] = 'Location';
$handler->display->display_options['fields']['field_geofield']['element_label_type'] = 'strong';
$handler->display->display_options['fields']['field_geofield']['click_sort_column'] = 'wkt';
$handler->display->display_options['fields']['field_geofield']['type'] = 'geofield_latlon';
$handler->display->display_options['fields']['field_geofield']['settings'] = array(
  'data' => 'full',
  'format' => 'decimal_degrees',
  'labels' => 1,
);
/* Campo: Contenuto: Link modifica */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['text'] = 'modifica';
/* Criterio di ordinamento: Contenuto: Data di inserimento */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Contenuto: Pubblicato */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Contenuto: Tipo */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'geocontent' => 'geocontent',
);

/* Display: Page Google Map */
$handler = $view->new_display('page', 'Page Google Map', 'page');
$handler->display->display_options['path'] = 'geocontents-gmap';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Google Map';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Page Leaflet Map */
$handler = $view->new_display('page', 'Page Leaflet Map', 'page_1');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'ip_geoloc_leaflet';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => '',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['ip_geoloc_views_plugin_latitude'] = 'field_geofield';
$handler->display->display_options['style_options']['ip_geoloc_views_plugin_longitude'] = 'field_geofield';
$handler->display->display_options['style_options']['center_option'] = '0';
$handler->display->display_options['style_options']['map_height'] = '600';
$handler->display->display_options['style_options']['map_options']['maxzoom'] = '18';
$handler->display->display_options['style_options']['map_options']['zoom'] = '2';
$handler->display->display_options['style_options']['map_options']['scrollwheelzoom'] = '1';
$handler->display->display_options['style_options']['map_options']['dragging'] = '1';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['path'] = 'geocontents-leafletmap';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Leaflet Map';
$handler->display->display_options['menu']['weight'] = '1';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['geocontents_map'] = array(
  t('Master'),
  t('Geocontents Map'),
  t('leggi tutto'),
  t('Applica'),
  t('Ripristina'),
  t('Ordina per'),
  t('Asc'),
  t('Disc'),
  t('<h3>Ancora nessun Contenuto inputato sulla mappa !</h3>'),
  t('Tipo'),
  t('Descrizione'),
  t('Location'),
  t('modifica'),
  t('Page Google Map'),
  t('Page Leaflet Map'),
);


/* Define this new view from code in views list */
$views[$view->name] = $view;