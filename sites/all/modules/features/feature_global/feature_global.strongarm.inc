<?php
/**
 * @file
 * feature_global.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_global_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_name';
  $strongarm->value = 'drupal-7-course 2nd day';
  $export['site_name'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_slogan';
  $strongarm->value = 'Drupal 7 Playground';
  $export['site_slogan'] = $strongarm;

  return $export;
}
